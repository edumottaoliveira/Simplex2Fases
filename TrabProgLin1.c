/*
 ============================================================================
 Name        : TrabProgLin1.c
 Author      :
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

enum Status { ERRO_ENTRA, ERRO_SAI, SUCESSO};

void printTableau(double** tableau, int m, int n, int fase) {
	printf("\n");
	for (int j = 0; j < n-2; j++) {
		printf(" X%d\t", j);
	}
	printf(" b\t\n");

	for (int j = 0; j < n; j++)
		printf("--------");
	printf("\n");

	if (fase == 2) {
		for (int j = 0; j < n-1; j++) {
			printf("%0.2lf\t", tableau[0][j]);
		}
		printf("Z\n");

		for (int j = 0; j < n; j++)
			printf("--------");
		printf("\n");

		for (int i = 1; i < m; i++) {
			for (int j = 0; j < n-1; j++) {
				printf("%0.2lf\t", tableau[i][j]);
			}
			int varBasica = (int)tableau[i][n-1];
			if (varBasica < 0) {
				printf("-\n");
			}
			else {
				printf("X%d\n", varBasica);
			}
		}
	}
	else {

		for (int j = 0; j < n-1; j++) {
			printf("%0.2lf\t", tableau[0][j]);
		}
		printf("Za\n");

		for (int j = 0; j < n-1; j++) {
			printf("%0.2lf\t", tableau[1][j]);
		}
		printf("Z\n");

		for (int j = 0; j < n; j++)
			printf("--------");
		printf("\n");

		for (int i = 2; i < m; i++) {
			for (int j = 0; j < n-1; j++) {
				printf("%0.2lf\t", tableau[i][j]);
			}
			int varBasica = (int)tableau[i][n-1];
			if (varBasica < 0) {
				printf("-\n");
			}
			else {
				printf("X%d\n", varBasica);
			}
		}
	}



}

double** alocarTableau(int m, int n) {
	double** tableau = (double**) malloc(m * sizeof(double*));
	for (int i = 0; i < m; i++) {
		tableau[i] = (double*) calloc(n, sizeof(double));
	}
	return tableau;
}

void freeTableau(double** tableau, int m, int n) {
	for (int i = 0; i < m; i++) {
		free(tableau[i]);
	}
	free(tableau);
}

int isIgual(double a, double b) {
	//printf("%lf == %lf ? %d\n", a, b, fabs(a - b) < 0.000001);
	return (fabs(a - b) < 0.000001);
}

void trocaSinalLinha(double** tableau, int linha, int n) {
	for (int j = 0; j < n-1; j++) {
		if (!isIgual(tableau[linha][j], 0.0)) {
			tableau[linha][j] *= (-1);
		}
	}
}

int contarVarArtificiais(double** tableau, int n) {
	int contador = 0;
	for (int j = 0; j < n-1; j++) {
		if (!isIgual(tableau[0][j], 0.0))
			contador++;
	}
	return contador;
}

int haVarArtificialNaBase(int numVarArtificiais, double** tableau, int m, int n) {
	for (int i = 2; i < m; i++) {
		int varBasica = (int)tableau[i][n-1];
		if (varBasica <= n-3 && varBasica >= n-2-numVarArtificiais) {
			printf("\nEncontrado var artificial X%d na base!\n", varBasica);
			return 1;
		}
	}
	return -1;
}

int linhaPrimeiraVarArtificialNaBase(int numVarArtificiais, double** tableau, int m, int n) {
	for (int i = 2; i < m; i++) {
		int varBasica = (int)tableau[i][n-1];
		if (varBasica <= n-3 && varBasica >= n-2-numVarArtificiais) {
			return i;
		}
	}
	return -1;
}

int haVarArtificialPositivaNaBase(int numVarArtificiais, double** tableau, int m, int n) {
	for (int i = 2; i < m; i++) {
		int varBasica = (int)tableau[i][n-1];
		if (varBasica <= n-3 && varBasica >= n-2-numVarArtificiais) {
			// varBasica eh artificial. varBasica eh positiva?
			if (tableau[i][n-2] > 0.0) {
				return 1;
			}
		}
	}
	return -1;
}

void anulaLinhaVarArtificiaisDaBase(int numVarArtificiais, double** tableau, int m, int n) {
	for (int i = 2; i < m; i++) {
			int varBasica = (int)tableau[i][n-1];
			if (varBasica <= n-3 && varBasica >= n-2-numVarArtificiais) {
				// varBasica eh artificial. sua linha eh nula (com excecao de seu 1)?
				double soma = 0.0;
				for (int j = 0; j < n-2; j++) {
					soma += tableau[i][j];
				}
				if (isIgual(soma, 1.0) && isIgual(tableau[i][varBasica], 1.0)) {
					tableau[i][varBasica] = 0.0;
					tableau[i][n-1] = -1.0;
				}
			}
		}
}

void zerarColunaVarArtificiaisNaoBasicas(int numVarArtificiais, double** tableau, int m, int n) {
	for (int j = n-2-numVarArtificiais; j < n-2; j++) {
			int isVarBasica = -1;
			for (int i = 2; i < m; i++) {
				int varBasica = (int)(tableau[i][n-1]);
				if (j == varBasica) {
					isVarBasica = 1;
					break;
				}
			}

			if (isVarBasica == -1) {
				for (int i = 0; i < m; i++) {
					tableau[i][j] = 0.0;
				}
			}

	}
}

int existeVarNaoBasicaComZNulo(double** tableau, int m, int n) {
	int i, j, isBasica;
	for (j = 0; j < n-2; j++) {  // para cada var
		isBasica = -1;

		for (i = 1; i < m; i++) {

			if ( j == (int)tableau[i][n-1] ) {
				isBasica = 1;
				break;
			}
		}

		if (isBasica == -1) {
			if ( isIgual(tableau[0][j], 0.0) ) {
				return 1;
			}
		}
	}

	return -1;
}

int existeVarBasicaComValorNulo(double** tableau, int m, int n) {
	int existeNulo = -1;
	for (int i = 1; i < m; i++) {
		//printf(" (%d, %d) %lf == 0\n", i, n-2, tableau[i][n-2]);
		if (isIgual(tableau[i][n-2], 0.0)) {
			return 1;
		}
	}
	return -1;
}


void removerVarArtificiasDoTableau(double** tableau, int m, int n, int numVarArtificiais) {
	// Remove 1a linha
	for (int i = 0; i < m-1; i++) {
		for (int j = 0; j < n; j++) {
			//printf("(%d, %d) <- (%d, %d)\n", i, j, i+1, j);
			tableau[i][j] = tableau[i+1][j];
		}
	}
	// Remove colunas
	int colPrimeiraVarArt = n-2-numVarArtificiais;
	for (int j = colPrimeiraVarArt; j < colPrimeiraVarArt + numVarArtificiais; j++) {
		//printf("(col %d) <- (col %d)\n", j, j+numVarArtificiais);
		for (int i = 0; i < m-1; i++) {
			tableau[i][j] = tableau[i][j+numVarArtificiais];
		}
	}
}

// Se canonica retorna posicao do 1, senao retorna -1
int isColCanonica(double** tableau, int j, int iInicio, int iFim) {

	double soma = 0.0;
	for (int i = iInicio; i <= iFim; i++) {
		soma += tableau[i][j];
	}

	if (isIgual(soma, 1.0)) {
		// procura pos do 1 ou verifica se so ha um 1
		int posUm = -1;
		for (int i = iInicio; i <= iFim; i++) {

			if (isIgual(tableau[i][j], 1.0)) {

				if (posUm == -1) {
					posUm = i;

				}
				else {
					return -1;
				}
			}
		}
		// Base canonica ok. Retornando pos 1
		return posUm;
	}
	else {
		return -1;
	}
	return -1;
}



void gerarBasesCanonicasIniciaisArtificiais(double** tableau, int m, int n) {
	int i, j;

	// Preenche col var basicas
	int posUm = -1;
	for (j = 0; j < n-2; j++) {
		posUm = isColCanonica(tableau, j, 2, m-1);
		if (posUm > 0) {
			tableau[posUm][n-1] = (double)j;
		}
	}

	printf("\nInicio com var artificiais na base:\n");
	printTableau(tableau, m, n, 1);


	for (i = 2; i < m; i++) {
		int varBasica = (int) tableau[i][n-1];
		if ( !isIgual(tableau[0][varBasica], 0.0) ) {

			double c = tableau[0][varBasica] / tableau[i][varBasica];
			for (j = 0; j < n-1; j++) {
				//printf("(%lf - %lf) \n",tableau[0][j], tableau[i][j]);
				tableau[0][j] -= c * tableau[i][j];
			}
		}
	}

	printf("\nPivoteando as colunas das var basicas:\n");
	printTableau(tableau, m, n, 1);
}

int existePositivoLinha(int linha, double** tableau, int n) {
	for (int j = 0; j < n-1; j++) {
		if (tableau[linha][j] > 0) {
			return 1;
		}
	}
	return 0;
}


int maiorPositivoLinha(int linha, double** tableau, int n) {
	int indiceMaior = 0;
	for (int j = 0; j < n-2; j++) {
		if (tableau[linha][j] > tableau[linha][indiceMaior]) {
			indiceMaior = j;
		}
	}
	if (tableau[linha][indiceMaior] > 0.0) {
		return indiceMaior;
	}
	return -1;
}

// Retorna indice da linha com menor divisao > 0
int menorDivisaoPositivaColCol(double** tableau, int iInicio, int iFim, int colB, int colY) {
	int indiceMenorDivisao = -1;
	double divisao, menorDivisao = 100000000000000;
	for (int i = iInicio; i <= iFim; i++) {
		//printf("menor divisao (%d,%d)? \n", i, colY);
		if ( (!isIgual(tableau[i][colY], 0.0)) && tableau[i][colY] > 0.0 ) {
			if ( isIgual(tableau[i][colB], 0.0) ) {
				//printf("(%d,%d) = %lf = 0 \n", i, colY, tableau[i][colB]);
				if ( 0.0 < menorDivisao ) {
					menorDivisao = 0.0;
					indiceMenorDivisao = i;
				}
			}
			else {
				divisao = tableau[i][colB]/tableau[i][colY];
				if ( divisao <= menorDivisao && divisao >= 0.0) {    // CUIDADO!  >= 0 pode dar problema
					menorDivisao = tableau[i][colB]/tableau[i][colY];
					indiceMenorDivisao = i;
					//printf("menor divisao (%d,%d): %lf / %lf\n", i , colY, tableau[i][colB], tableau[i][colY]);
				}
			}
		}
	}
	return indiceMenorDivisao;
}

void divideLinhaPorConstante(double** tableau, int linha, int n, double constante) {

	for (int j = 0; j < n-1; j++) {
		tableau[linha][j] /= constante;
	}
}

void pivoteia(double** tableau, int m, int n, int iPivot, int jPivot) {
	for (int i = 0; i < m; i++) {
		if (i != iPivot) {
			double c = tableau[i][jPivot]/tableau[iPivot][jPivot];
			for (int j = 0; j < n-1; j++) {
				//printf("%lf - (%lf * %lf)\n", tableau[i][j], c ,tableau[iPivot][j]);
				tableau[i][j] -= c * tableau[iPivot][j];
			}
		}
	}

}

int trocaVariavelNaBasePrimFase(double** tableau, int m, int n) {
	int j = maiorPositivoLinha(0, tableau, n);
	if (j == -1) {
		return ERRO_ENTRA;
	}
	int i = menorDivisaoPositivaColCol(tableau, 2, m-1, n-2, j);

	printf("\nNovo pivot: (%d, %d)\n", i, j);

	if (i >= 0) {
		printf("Divide linha %d por %lf e pivoteia\n", i, tableau[i][j]);
		divideLinhaPorConstante(tableau, i, n, tableau[i][j]);
		tableau[i][n-1] = (double)j;

		pivoteia(tableau, m, n, i, j);
		printTableau(tableau, m, n, 1);
	}
	else {
		return ERRO_SAI;
	}

	return SUCESSO;


	// Procura outro positivo linha
}


int trocaVarArtificialParaTirarDaBase(double** tableau, int m, int n, int numVarArtificiais) {
	int i = linhaPrimeiraVarArtificialNaBase(numVarArtificiais, tableau, m, n);
	if (i < 0) {
		return -1;
	}
	int j = 0;
	for (j = 0; j < n-2-numVarArtificiais; j++){
		if (isIgual(tableau[i][j], 0.0) == 0) {
			break;
		}
	}


	if (j == n-2-numVarArtificiais) {
		return -1;
	}

	printf("\nNovo pivot: (%d, %d)\n", i, j);
	printf("Divide linha %d por %lf e pivoteia\n", i, tableau[i][j]);
	divideLinhaPorConstante(tableau, i, n, tableau[i][j]);
	tableau[i][n-1] = (double)j;

	pivoteia(tableau, m, n, i, j);
	printTableau(tableau, m, n, 1);

	return SUCESSO;
}

int trocaVariavelNaBaseSegFase(double** tableau, int m, int n) {
	int j = maiorPositivoLinha(0, tableau, n);
	if (j == -1) {
		return ERRO_ENTRA;
	}
	int i = menorDivisaoPositivaColCol(tableau, 1, m-1, n-2, j);

	printf("\nNovo pivot: (%d, %d)\n", i, j);

	if (i >= 0) {
		printf("Divide linha %d por %0.2lf e pivoteia\n", i, tableau[i][j]);
		divideLinhaPorConstante(tableau, i, n, tableau[i][j]);
		tableau[i][n-1] = (double)j;

		pivoteia(tableau, m, n, i, j);
		printTableau(tableau, m, n, 2);
	}
	else {
		return ERRO_SAI;
	}

	return SUCESSO;
}

void printSolucao(double** tableau, int m, int n) {
	double valorVar;
	printf("\n x* = ( ");
	for (int x = 0; x < n-2; x++) {
		for (int i = 1; i < m; i++) {
			valorVar = 0.0;
			if ( isIgual(tableau[i][n-1], (double)x) ){
				valorVar = tableau[i][n-2];
				break;
			}
		}
		printf("%0.3lf ", valorVar);
	}
	printf(")\n");
	printf(" z* = %0.3lf\n", tableau[0][n-2]);
}

int main(int argc, char *argv[]) {

	if (argc != 2) {
		printf("\n Erro! Sem arquivo de entrada.");
		printf("\n Modo de usar: ./simplex <arquivo_do_problema> \n");
		return 0;
	}

	int numFases = 0;
	int m = 0, n = 0;
	double** tableau;

	// LEITURA
	char* buffer = (char*) calloc(256, sizeof(double));
	FILE* arq = fopen(argv[1], "r");
	if (arq == NULL) {
		printf("Erro: Arquivo nao encontrado para leitura.");
		exit(1);
	}

	fscanf(arq,"%d", &numFases);
	printf("numFases: %d\n", numFases);

	fscanf(arq,"%d %d", &m, &n);
	n++; // Para acomodar indices das var basicas
	tableau = alocarTableau(m, n);
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n-1; j++) {
			fscanf(arq,"%lf", &tableau[i][j]);
		}
	}

	//printTableau(tableau, m, n);
	free(buffer);
	fclose(arq);
	int N = n;
	int M = m;
	int status;


	//------------------//
	//      1a FASE		//
	//------------------//
	if (numFases == 2) {
		int numVarArtificiais = contarVarArtificiais(tableau, n);
		printf("var artificiais: %d\n", numVarArtificiais);
		trocaSinalLinha(tableau, 0, n);
		trocaSinalLinha(tableau, 1, n);
		gerarBasesCanonicasIniciaisArtificiais(tableau, m, n);

		status = SUCESSO;
		while (status == SUCESSO) {
			status = trocaVariavelNaBasePrimFase(tableau, m, n);
		}

		switch (status) {
		case ERRO_ENTRA:
			// Funcao objetivo artifical  == 0 ?
			if (isIgual(tableau[0][n-2], 0.0)) {
				if (haVarArtificialNaBase(numVarArtificiais, tableau, m, n) == 1) {

					if (haVarArtificialPositivaNaBase(numVarArtificiais, tableau, m, n) == 1) {
						printf("\nSimplex terminado: Var artificial na base apos 1a fase.\n (Sem solucao)");
						printTableau(tableau, m, n, 1);
						freeTableau(tableau, M, N);
						exit(0);
					}
					else {
						printf("Tentando eliminar var artificiais da base.\n");
						zerarColunaVarArtificiaisNaoBasicas(numVarArtificiais, tableau, m, n);
						// Elimina var basicas se posivel
						anulaLinhaVarArtificiaisDaBase(numVarArtificiais, tableau, m, n);

						// Se ainda ha variaveis
						if (haVarArtificialNaBase(numVarArtificiais, tableau, m, n) == 1) {
							printf("Ha ainda var artificial na base! Removendo por substituicao.\n");

							int iVarArtificial = linhaPrimeiraVarArtificialNaBase(numVarArtificiais, tableau, m, n);

							int l = 0, limite = 10;
							while (iVarArtificial != -1 && l < limite) {
								trocaVarArtificialParaTirarDaBase(tableau, m, n, numVarArtificiais);
								iVarArtificial = linhaPrimeiraVarArtificialNaBase(numVarArtificiais, tableau, m, n);
								l++;
							}

							removerVarArtificiasDoTableau(tableau, m, n, numVarArtificiais);
							n = n - numVarArtificiais;
							m = m - 1;
							printf("\nNovo Tableau sem as variaveis artificiais\n");
							printTableau(tableau, m, n, 2);

						}
						else {
							removerVarArtificiasDoTableau(tableau, m, n, numVarArtificiais);
							n = n - numVarArtificiais;
							m = m - 1;
							printf("\nNovo Tableau sem as variaveis artificiais\n");
							printTableau(tableau, m, n, 2);

						}


					}


				}
				else {
					printf("\n-> Iniciar 2a Fase do Simplex:\n");
					removerVarArtificiasDoTableau(tableau, m, n, numVarArtificiais);
					n = n - numVarArtificiais;
					m = m - 1;
					printf("\nNovo Tableau sem as variaveis artificiais\n");
					printTableau(tableau, m, n, 2);
				}

			}
			else {
				printf("\nSimplex terminado: Nao ha solucao.\n(Funcao objetivo artificial nao zerada)\n");
				printTableau(tableau, m, n, 1);
				freeTableau(tableau, M, N);
				exit(0);
			}
			break;

		case ERRO_SAI:
			// TODO
			break;

		}
	}


	//------------------//
	//      2a FASE		//
	//------------------//

	if (numFases == 1) {
		trocaSinalLinha(tableau, 0, n);

		// Preenche col var basicas
		int posUm;
		for (int j = 0; j < n-2; j++) {
			posUm = isColCanonica(tableau, j, 1, m-1);
			if (posUm > 0) {
				tableau[posUm][n-1] = (double)j;
			}
		}
		printf("\nInicio:\n");
		printTableau(tableau, m, n, 2);
	}

	status = SUCESSO;
	int i = 0;
	while (status == SUCESSO && i < 10) {
		status = trocaVariavelNaBaseSegFase(tableau, m, n);
		i++;
	}

	switch (status) {
		case ERRO_ENTRA:
			if (existeVarNaoBasicaComZNulo(tableau, m, n) == 1) {
				printf("\nSimplex terminado: Solucao Multipla (infinitas solucoes)\nUma solucao otima:");
			}
			else if (existeVarBasicaComValorNulo(tableau, m, n) == 1) {
				printf("\nSimplex terminado: Solucao Degenerada\n");
			}
			else {
				printf("\nSimplex terminado: Solucao Unica\n");
			}
			printSolucao(tableau, m, n);
			printTableau(tableau, m, n, 2);
			freeTableau(tableau, M, N);
			exit(0);
			break;

		case ERRO_SAI:
			printf("\nSimplex terminado: Solucao eh ilimitada.\n(Sem otimo)\n");
			printTableau(tableau, m, n, 2);
			freeTableau(tableau, M, N);
			exit(0);
			break;
	}








	return EXIT_SUCCESS;
}
